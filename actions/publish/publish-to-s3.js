define(function(require, exports, module) {

    var UI = require("ui");
    var ContentHelpers = require("content-helpers");

    return UI.registerAction("publish-to-s3", UI.AbstractUIAction.extend({

        defaultConfiguration: function() {

            var config = this.base();

            config.title = "Publish to S3";
            config.iconClass = "fa fa-plus";
            return config;
        },

        prepareAction: function(actionContext, config, callback) {

            actionContext.currentPath = actionContext.observable("path").get();

            callback();
        },

        executeAction: function(actionContext, config, callback) {

            var self = this;

            console.log("Inside executeAction");
            console.log("actionContext: " + JSON.stringify(actionContext));

        },

        createHandler: function(actionContext, props, callback) {
            console.log("Inside createHandler");
        }

    }));
});
